zita-rev1 (0.2.2-2) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: Pass a triplet-prefixed pkg-config. (Closes: #949990)

  [ Dennis Braun ]
  * Add a bigger icon
  * Add me as uploader
  * Bump dh-compat to 13
  * d/rules: Remove -Wl,--as-needed ldflags

 -- Dennis Braun <d_braun@kabelmail.de>  Sun, 09 Aug 2020 21:39:01 +0200

zita-rev1 (0.2.2-1) unstable; urgency=medium

  * Team upload

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Dennis Braun ]
  * New upstream release 0.2.2
  * d/control:
    + Bump debhelper-compat to 12
    + Bump standards version to 4.5.0
    + Use https protocol for homepage
    + Add libfreetype6-dev and pkg-config to B-D
    + Set RRR: no
  * d/copyright: Update year, GPL2+ text, http > https & add myself
  * d/patches: Update 01-makefile.patch
    + Add fixed pkgconf installation
  * d/source/local-options: Drop, obsolete
  * d/watch: Use https protocol

 -- Dennis Braun <d_braun@kabelmail.de>  Mon, 27 Jan 2020 22:53:37 +0100

zita-rev1 (0.2.1-5) unstable; urgency=medium

  * Set dh/compat 10.
  * Sign tags.
  * Avoid useless linking.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 27 Dec 2016 16:29:59 +0100

zita-rev1 (0.2.1-4) unstable; urgency=medium

  * Bump Standards.
  * Fix VCS fields.
  * Remove menu file.
  * Fix hardening.
  * Add french man pages.
  * Add french to desktop file. (Closes: #825042)
  * Tune .gitignore file.
  * Update copyright file.
  * Enable parallel build.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Mon, 08 Aug 2016 18:18:16 +0200

zita-rev1 (0.2.1-3) unstable; urgency=low

  * Set dh/compat 9
  * Removed DMUA.
  * Don't sign tags.
  * Fix VCS canonical URLs.
  * Bump standards.
  * Tighten build-deps.
  * Update copyright file.
  * Added Keywords entry to desktop file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Mon, 12 Aug 2013 22:59:01 +0200

zita-rev1 (0.2.1-2) unstable; urgency=low

  * Team upload.
  * libpng12-dev -> libpng-dev (Closes: #662577)
  * Set DMUA to yes.
  * Remove myself from the Uploaders field.
  * Update debian/copyright.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Thu, 12 Apr 2012 00:51:41 +0200

zita-rev1 (0.2.1-1) unstable; urgency=low

  * New upstream release.
  * Update debian/copyright.
  * Refresh patches.
  * Build-dep on libjack-dev only.

 -- Alessio Treglia <alessio@debian.org>  Fri, 17 Jun 2011 11:34:20 +0200

zita-rev1 (0.1.1-3) unstable; urgency=low

  * Icon added

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Fri, 27 May 2011 19:20:07 +0200

zita-rev1 (0.1.1-2) unstable; urgency=low

  * Don't compile with -march=native, fix FTBFSs.

 -- Alessio Treglia <alessio@debian.org>  Tue, 24 May 2011 18:08:12 +0200

zita-rev1 (0.1.1-1) unstable; urgency=low

  * Initial release (Closes: #627255).

 -- Alessio Treglia <alessio@debian.org>  Thu, 19 May 2011 09:53:23 +0200
